package main

import (
	"os"
	"strings"

	"gitlab.com/bjjb/pungi"
)

type cmd = pungi.T

func main() {
	noNewLine := false
	pungi.New(
		"echo",
		pungi.Run(func(c cmd, args ...string) {
			c.Printf(strings.Join(args, " "))
			if !noNewLine {
				c.Printf("\n")
			}
		}),
		pungi.Bool(&noNewLine, "n", "no-newline", "don't print a new line"),
	).Execute(os.Args[1:]...)
}
