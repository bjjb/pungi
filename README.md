# pungi 🐍

A Go library for building command-line applications.

`pungi` wraps the excellent [spf13/cobra](https://cobra.dev)
library to provide a builder interface and a functional interface to build up
complex command-line processors with sub-commands and shell completion.
