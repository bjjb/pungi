// Package pungi provides a builder pattern and a functional interface for
// building spf13/cobra commands. To use the builder pattern, you'd initialize
// a new Builder and mofidify it in place, before executing it (in, say, a
// `main()` function). To use the functional interface, you would use the
// package functions to get modifiers, and use those on the Builder, either by
// calling the resulting modifier function on the Pungi iteself, or by simply
// passing them in as additional arguments to a `New` invokation.
package pungi

import (
	"io"

	"github.com/spf13/cobra"
)

// A T has functions to build commands with flags and subcommands.
type T interface {
	Version(string) T
	Summary(string) T
	Aliases(...string) T
	Add(...T) T
	Stdin(io.Reader) T
	Stdout(io.Writer) T
	Stderr(io.Writer) T
	Description(string) T
	Bool(*bool, string, string, string) T
	String(*string, string, string, string) T
	Run(...func(T, ...string)) T
	Printf(string, ...interface{})
	PrintErr(...interface{})
	Execute(...string)
	command() *cobra.Command
}

// Version is a command modifier which sets a version
func Version(s string) func(T) {
	return func(c T) {
		c.command().Version = s
	}
}

// Summary is a command modifier which sets a summary
func Summary(s string) func(T) {
	return func(c T) {
		c.command().Short = s
	}
}

// Description is a command modifier which sets a description
func Description(s string) func(T) {
	return func(c T) {
		c.command().Long = s
	}
}

// Aliases is a command modifier which sets aliases.
func Aliases(s ...string) func(T) {
	return func(c T) {
		c.command().Aliases = s
	}
}

// Stdin is a command modifier which sets the standard input reader
func Stdin(r io.Reader) func(T) {
	return func(c T) {
		c.command().SetIn(r)
	}
}

// Stdout is a command modifier which sets the standard output writer
func Stdout(w io.Writer) func(T) {
	return func(c T) {
		c.command().SetOut(w)
	}
}

// Stderr is a command modifier which sets the standard error writer
func Stderr(w io.Writer) func(T) {
	return func(c T) {
		c.command().SetErr(w)
	}
}

// Bool is a command modifier which adds a boolean flag
func Bool(v *bool, short, long, desc string) func(T) {
	return func(c T) {
		c.command().PersistentFlags().BoolVarP(v, long, short, *v, desc)
	}
}

// String is a command modifier which adds a string flag
func String(v *string, short, long, desc string) func(T) {
	return func(c T) {
		c.command().PersistentFlags().StringVarP(v, long, short, *v, desc)
	}
}

// Add is a command modifier which adds commands
func Add(cmds ...T) func(c T) {
	return func(c T) {
		for _, cmd := range cmds {
			c.command().AddCommand(cmd.command())
		}
	}
}

// Run is a modifier to set the function which is run when the command is
// executed.
func Run(f ...func(T, ...string)) func(c T) {
	return func(c T) {
		c.command().Run = func(cmd *cobra.Command, args []string) {
			for _, f := range f {
				f(c, args...)
			}
		}
	}
}

// A pungi wraps a *cobra.Command and provides some convenience methods for
// building it up, and a nicer Execute method.
type pungi struct {
	*cobra.Command
}

// Version sets the version of the Command and returns the Command.
func (c *pungi) Version(s string) T {
	Version(s)(c)
	return c
}

// Summary sets the summary of the Command and returns the Command.
func (c *pungi) Summary(s string) T {
	Summary(s)(c)
	return c
}

// Description sets the description of the Command and returns the Command.
func (c *pungi) Description(s string) T {
	Description(s)(c)
	return c
}

// Aliases sets the aliases of the Command and returns the Command.
func (c *pungi) Aliases(s ...string) T {
	Aliases(s...)(c)
	return c
}

// Bool adds a boolean flag to the Command and returns the Command.
func (c *pungi) Bool(v *bool, short, long, desc string) T {
	Bool(v, short, long, desc)(c)
	return c
}

// String adds a string flag to the Command and returns the Command.
func (c *pungi) String(v *string, short, long, desc string) T {
	String(v, short, long, desc)(c)
	return c
}

// Add adds Commands to the Command and returns the Command.
func (c *pungi) Add(cmds ...T) T {
	Add(cmds...)(c)
	return c
}

// Stdin sets the standard input of the Command and returns the Command.
func (c *pungi) Stdin(r io.Reader) T {
	Stdin(r)(c)
	return c
}

// Stdout sets the standard output of the Command and returns the Command.
func (c *pungi) Stdout(w io.Writer) T {
	Stdout(w)(c)
	return c
}

// Stderr sets the standard error of the Command and returns the Command.
func (c *pungi) Stderr(w io.Writer) T {
	Stderr(w)(c)
	return c
}

// Run sets the function that gets run when the command is executed.
func (c *pungi) Run(f ...func(T, ...string)) T {
	Run(f...)(c)
	return c
}

// Execute sets the command's arguments and executes it.
func (c *pungi) Execute(args ...string) {
	c.command().SetArgs(args)
	c.command().Execute()
}

// cmd returns the underlying command.
func (c *pungi) command() *cobra.Command {
	return c.Command
}

// New builds a new Command.
func New(name string, f ...func(c T)) T {
	c := &pungi{&cobra.Command{Use: name}}
	for _, f := range f {
		f(c)
	}
	return c
}
